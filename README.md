Directions
==========

Please markup the test.psd into valid HTML5 responsive markup.

- Semantic markup that looks good even without CSS applied
- Please use a jQuery slider plugin for the background slider. There's only one image given for the slider. Please pick 2-3 other images of your choice for the background slider
- Use of HTML5 elements where appropriate
- Please use a CSS preprocessor like SASS, LESS, or stylus.
- Use of jQuery to provide interactivity, again keeping in mind how the page will appear to those without JavaScript enabled
- Use of web fonts for where appropriate. If you can find the exact webfont, please pick one that looks close.
- Your layout should be usable down to a 320px width. A desktop .psd layout will only be provided, so use your best judgment to produce a useful and logical small-screen layout. 
